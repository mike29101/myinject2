package de.fastr.phonegap.plugins;

import android.content.Context;
import android.util.Log;

import org.apache.cordova.CordovaPreferences;
import android.content.Context;
import android.content.res.AssetManager;
import android.util.AttributeSet;
import android.util.Log;
import org.crosswalk.engine;
import org.xwalk.core.XWalkView;
import org.xwalk.core.XWalkResourceClient;
import org.xwalk.core.XWalkUIClient;
import org.xwalk.core.XWalkCookieManager;
import org.xwalk.core.XWalkNavigationHistory;
import org.apache.cordova.engine.SystemWebView;
import org.apache.cordova.engine.SystemWebViewEngine;

/**
 * Created by fabian on 22.05.15.
 */
public class InjectWebViewEngine extends XWalkView {

    /** Used when created via reflection. */
    public InjectWebViewEngine(Context context, CordovaPreferences preferences) {
        this(new InjectWebView(context));
        //super(context, preferences);
        Log.w("inject", "InjectWebViewEngine");

    }

    public InjectWebViewEngine(XWalkView webView) {
        super(webView);
        webView.setWebViewClient(new InjectWebViewClient(this));
    }

}